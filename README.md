# Front-end Exercise

## Prerequisites

- Node v10+
- NPM or Yarn

## Instructions

1. `npm install` or `yarn install`
1. `npm run serve` or `yarn serve`
